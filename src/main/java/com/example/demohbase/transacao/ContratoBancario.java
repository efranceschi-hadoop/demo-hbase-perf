package com.example.demohbase.transacao;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-26.
 */
@Data
@AllArgsConstructor
class ContratoBancario implements Contrato {
	private String agencia;
	private String conta;
}
