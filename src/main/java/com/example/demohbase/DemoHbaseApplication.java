package com.example.demohbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class DemoHbaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoHbaseApplication.class, args);
	}

}
