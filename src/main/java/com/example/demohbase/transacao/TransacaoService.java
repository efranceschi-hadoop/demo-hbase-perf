package com.example.demohbase.transacao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Service
public class TransacaoService {

	private final TransacaoRepository<TransacaoBancaria> transacaoBancariaRepository;

	public TransacaoService(
			@Qualifier("transacaoBancariaRepository") TransacaoRepository<TransacaoBancaria> transacaoBancariaRepository
	) {
		this.transacaoBancariaRepository = transacaoBancariaRepository;
	}

	public List<TransacaoBancaria> consultarTransacaoBancaria(String cpf, String agencia, String conta, Date dataInicio, Date dataTermino) {
		return transacaoBancariaRepository.consultar(cpf, new ContratoBancario(agencia, conta), dataInicio, dataTermino);
	}

	public List<TransacaoBancaria> consultarTransacaoBancaria(String cpf, Date dataInicio, Date dataTermino) {
		return transacaoBancariaRepository.consultar(cpf, dataInicio, dataTermino);
	}

	public void salvarTransacoesBancarias(List<TransacaoBancaria> transacoes) {
		transacaoBancariaRepository.salvar(transacoes);
	}

	public boolean isTableExists() {
		return transacaoBancariaRepository.isTableExists();
	}

}
