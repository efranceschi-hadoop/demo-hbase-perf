package com.example.demohbase;

import com.example.demohbase.config.AppSample;
import com.example.demohbase.transacao.TransacaoBancaria;
import com.example.demohbase.transacao.TransacaoService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-04.
 */
@Component
public class ExtratoCLR implements CommandLineRunner {

	private static final Logger LOG = LoggerFactory.getLogger(ExtratoCLR.class);

	private static final String NUMBERS = "0123456789";

	private final TransacaoService transacaoService;

	private final AppSample appSample;

	private final Random r = new Random();

	private final List<String> cpfList = new ArrayList<>();

	public ExtratoCLR(TransacaoService transacaoService, AppSample appSample) {
		this.transacaoService = transacaoService;
		this.appSample = appSample;
	}

	@Override
	public void run(String... args) throws IOException {
		if (transacaoService.isTableExists()) return;
		carregaCPF();
		geraTransacoesBancarias();
	}

	private void carregaCPF() throws IOException {
		File cpfFile = new File("target/cpf.txt");
		if (cpfFile.exists()) {
			LOG.info("Carregando números de CPF de {}...", cpfFile.toPath());
			try (BufferedReader r = new BufferedReader(new FileReader(cpfFile))) {
				String cpf = r.readLine();
				while (cpf != null) {
					cpfList.add(cpf);
					cpf = r.readLine();
				}
			}
		} else {
			LOG.info("Gerando {} números de CPF e salvando em {}", appSample.getCpf(), cpfFile.toPath());
			try (BufferedWriter w = new BufferedWriter(new FileWriter(cpfFile))) {
				for (int i = 0; i < appSample.getCpf(); i++) {
					String cpf = generateCpf();
					w.write(cpf);
					w.write('\n');
					cpfList.add(cpf);
				}
			}
		}
		LOG.info("{} números de CPF carregados!", cpfList.size());
	}

	private void geraTransacoesBancarias() throws IOException {
		long startTime = System.currentTimeMillis();
		long transactionId = 0L;

		int totalPrevisto = appSample.getAgencias() * appSample.getContas() * appSample.getPeriodo() * appSample.getTransacoesDia();

		LOG.info("Quantidade de Agências: {}", appSample.getAgencias());
		LOG.info("Quantidade de Contas por Agência: {}", appSample.getContas());
		LOG.info("Período das transações: {} dias", appSample.getPeriodo());
		LOG.info("Quantidade de transações diárias: {}", appSample.getTransacoesDia());
		LOG.info("Gerando {} transações bancárias para {} contas correntes...", totalPrevisto, appSample.getAgencias() * appSample.getContas());

		List<TransacaoBancaria> transacoes = new ArrayList<>();

		Iterator<String> itCpf = cpfList.iterator();

		File contasFile = new File("target/contas.txt");
		try (BufferedWriter w = new BufferedWriter(new FileWriter(contasFile))) {

			for (int ag = 1; ag <= appSample.getAgencias(); ag++) {

				String agencia = StringUtils.leftPad(String.valueOf(ag), 6, "0");

				LOG.info(" * Gerando {} transações para a agência: {}", appSample.getContas() * appSample.getPeriodo() * appSample.getTransacoesDia(), agencia);

				for (int cc = 1; cc <= appSample.getContas(); cc++) {

					if (!itCpf.hasNext()) {
						itCpf = cpfList.iterator(); // Reinicia o iterator
					}

					String conta = StringUtils.leftPad(String.valueOf(cc), 10, "0");
					String cpf = itCpf.next();

					w.write(cpf + "," + agencia + "," + conta + "\n");

					LOG.info("   * Gerando {} transações para a conta: {} (CPF: {})", appSample.getPeriodo() * appSample.getTransacoesDia(), agencia + "/" + conta, cpf);

					for (int dia = 1; dia <= appSample.getPeriodo(); dia++) {
						Date data = data(dia);

						for (int t = 1; t <= appSample.getTransacoesDia(); t++) {

							double valor = generateValor();
							TransacaoBancaria transacaoBancaria = TransacaoBancaria
									.builder()
									.id(String.valueOf(++transactionId))
									.cpf(cpf)
									.agencia(agencia)
									.conta(conta)
									.data(data)
									.descricao((valor > 0 ? "CREDITO" : "DEBITO") + " EM CONTA")
									.valor(BigDecimal.valueOf(valor))
									.build();

							transacoes.add(transacaoBancaria);

							if (transactionId % 10000 == 0) {
								LOG.info("Salvando transações: {}/{} ({}%)", transactionId, totalPrevisto, String.format("%5.2f", (transactionId * 100d) / totalPrevisto));
								transacaoService.salvarTransacoesBancarias(transacoes);
								transacoes.clear();
							}

						}
					}

				}

			}
		}

		LOG.info("Salvando transações: {}/{}", transactionId, totalPrevisto);
		transacaoService.salvarTransacoesBancarias(transacoes);
		transacoes.clear();
		LOG.info("Carga finalizada: {} transações em {} segundos", transactionId, System.currentTimeMillis() - startTime);
	}

	private double generateValor() {
		return r.nextDouble() * 100;
	}

	private String generateCpf() {
		// CPF = 11
		char[] cpf = new char[11];
		for (int i = 0; i < cpf.length; i++) {
			cpf[i] = NUMBERS.charAt(r.nextInt(10));
		}
		return new String(cpf);
	}

	private Date data(int ref) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -ref);
		return c.getTime();
	}

}
