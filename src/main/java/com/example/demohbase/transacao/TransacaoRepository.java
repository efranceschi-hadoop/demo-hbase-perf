package com.example.demohbase.transacao;

import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
public interface TransacaoRepository<T extends Transacao> {

	List<T> consultar(String cpf, Contrato contrato, Date dataInicio, Date dataTermino);

	List<T> consultar(String cpf, Date dataInicio, Date dataTermino);

	void salvar(T transacao);

	void salvar(List<T> transacoes);

	boolean isTableExists();

}
