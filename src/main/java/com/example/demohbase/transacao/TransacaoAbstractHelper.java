package com.example.demohbase.transacao;

import com.example.demohbase.utils.HBaseHelper;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.compress.Compression;

import java.io.IOException;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-04.
 */
public abstract class TransacaoAbstractHelper<T extends Transacao> implements HBaseHelper<T> {

	private static final String CF_INFO = "i";
	private final String tableName;

	public TransacaoAbstractHelper(String tableName) {
		this.tableName = tableName;
	}

	private void createTable(TableName tableName, Admin admin) throws IOException {
		HTableDescriptor tableDescriptor = new HTableDescriptor(tableName);
		HColumnDescriptor cd = new HColumnDescriptor(CF_INFO);
		cd.setCompressionType(Compression.Algorithm.SNAPPY);
		tableDescriptor.addFamily(cd);
		admin.createTable(tableDescriptor);
	}

	@Override
	public Table getTable(Connection connection) throws IOException {
		TableName tableName = TableName.valueOf(this.tableName);
		Admin admin = connection.getAdmin();
		if (!admin.tableExists(tableName)) {
			createTable(tableName, admin);
		}
		return connection.getTable(tableName);
	}

	@Override
	public boolean isTableExists(Connection connection) throws IOException {
		TableName tableName = TableName.valueOf(this.tableName);
		Admin admin = connection.getAdmin();
		return admin.tableExists(tableName);
	}

}
