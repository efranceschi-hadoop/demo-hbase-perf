package com.example.demohbase.utils;

import org.apache.hadoop.hbase.client.Result;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
public class IteratorHelper<T> implements Iterator<T> {

	private final Iterator<Result> resultIterator;

	private final HBaseHelper<T> converter;

	public IteratorHelper(Iterator<Result> resultIterator, HBaseHelper<T> converter) {
		this.resultIterator = resultIterator;
		this.converter = converter;
	}

	@Override
	public boolean hasNext() {
		return resultIterator.hasNext();
	}

	@Override
	public T next() {
		return converter.value(resultIterator.next());
	}

	public List<T> toList() {
		List<T> result = new ArrayList<>();
		this.forEachRemaining(result::add);
		return result;
	}
}
