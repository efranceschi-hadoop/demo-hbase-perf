# Resultados obtidos nos testes

## Crit�rios de volume utilizados

| Volume  | CPF    | Agencias | Contas | Dias | Transa��es/Dia | Total       |
|---------|--------|----------|--------|------|----------------|-------------|
| Low     | 200    | 5        | 60     | 30   | 500            | 4.500.000   |
| Medium  | 200    | 5        | 60     | 30   | 1.000          | 9.000.000   |
| High    | 200    | 5        | 60     | 60   | 1.000          | 18.000.000  |
| Highest | 400    | 5        | 120    | 60   | 1.000          | 36.000.000  |
| Real    | 400    | 50       | 1200   | 60   | 2              | 7.200.000   |

## Estrat�gias de chave utilizadas

| Perfil    | Estrat�gia               | RowKey                       |
|-----------|--------------------------|------------------------------|
| Timestamp | Scan + Filtro por TS     | `<CPF>:<CONTA+AGENCIA>`      |
| Fuzzy     | Scan + Filtro Fuzzy      | `<CPF>:<TS>:<CONTA+AGENCIA>` |
| Client    | Scan + Filtro no Cliente | `<CPF>:<TS>:<CONTA+AGENCIA>` |

| Observa��o: Utilizado string reversa nos campos `CPF`, `AGENCIA` e `CONTA` 

## Chaves de busca que a aplica��o utiliza

* `CPF` e `TS`
* `CPF`, `AGENCIA`, `CONTA` e `TS`

## Tomadas de tempo m�dio

Foram realizadas **200 transa��es** em cada caso, em **3** s�ries, totalizando **600 transa��es**.

### Soma por CPF

| Perfil  | Timestamp | Fuzzy  | Client |
|---------|-----------|--------|--------|
| Real    | 59ms      | 49ms   | 57ms   |
| Low     | 361ms     | 350ms  | 346ms  |
| Medium  | 835ms     | 903ms  | 895ms  |
| High    | 961ms     | 863ms  | 830ms  |
| Highest | 1947ms    | 1711ms | 1689ms |

### Soma por CPF + Agencia + Conta

| Perfil  | Timestamp | Fuzzy  | Client |
|---------|-----------|--------|--------|
| Real    | 43ms      | 63ms   | 54ms   |
| Low     | 344ms     | 410ms  | 530ms  |
| Medium  | 577ms     | 639ms  | 871ms  |
| High    | 640ms     | 609ms  | 836ms  |
| Highest | 671ms     | 644ms  | 1699ms |
