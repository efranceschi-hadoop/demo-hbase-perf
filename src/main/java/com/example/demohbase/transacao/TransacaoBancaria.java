package com.example.demohbase.transacao;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-25.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class TransacaoBancaria extends TransacaoAbstract {

	private String agencia;

	private String conta;

	@Builder
	public TransacaoBancaria(String id, String cpf, Date data, String descricao, BigDecimal valor, String agencia, String conta) {
		super(id, cpf, data, descricao, valor);
		this.agencia = agencia;
		this.conta = conta;
	}

}
