package com.example.demohbase.transacao;

import com.example.demohbase.utils.ByteUtils;
import com.example.demohbase.utils.IteratorHelper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.FuzzyRowFilter;
import org.apache.hadoop.hbase.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.*;

import static com.example.demohbase.utils.ByteUtils.ONE;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Profile("fuzzy")
@Qualifier("transacaoBancariaRepository")
@Repository
public class TransacaoBancariaFuzzyRepositoryImpl implements TransacaoRepository<TransacaoBancaria> {

	private static final Logger LOG = LoggerFactory.getLogger(TransacaoBancariaFuzzyRepositoryImpl.class);

	private final Configuration configuration;
	private final TransacaoBancariaHelper helper;

	public TransacaoBancariaFuzzyRepositoryImpl(
			Configuration configuration,
			TransacaoBancariaHelper helper
	) {
		this.configuration = configuration;
		this.helper = helper;
	}

	@SuppressWarnings("Duplicates")
	@Override
	public List<TransacaoBancaria> consultar(String cpf, Contrato contrato, Date dataInicio, Date dataTermino) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			ContratoBancario contratoBancario = (ContratoBancario) contrato;

			String startRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataInicio)
					.build()
			);
			String stopRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataTermino)
					.build()
			);

			byte[] key = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataInicio)
					.agencia(contratoBancario.getAgencia())
					.conta(((ContratoBancario) contrato).getConta())
					.id("0")
					.build()
			).getBytes();

			byte[] mask = new byte[key.length];
			Arrays.fill(mask, 12, 26, ONE);
			Arrays.fill(mask, 44, 59, ONE);

			LOG.debug("  Chave de busca: {}", ByteUtils.prettyPrint(key));
			LOG.debug("Máscara de busca: {}", ByteUtils.toMask(mask));

			FuzzyRowFilter filter = new FuzzyRowFilter(Collections.singletonList(new Pair<>(key, mask)));
			Scan scan = new Scan();
			scan.setStartRow(startRow.getBytes());
			scan.setStopRow(stopRow.getBytes());
			scan.setFilter(filter);

			ResultScanner resultScanner = table.getScanner(scan);
			return new IteratorHelper<>(resultScanner.iterator(), helper).toList();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public List<TransacaoBancaria> consultar(String cpf, Date dataInicio, Date dataTermino) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			String startRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataInicio)
					.build()
			);
			String stopRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataTermino)
					.build()
			);
			Scan scan = new Scan();
			scan.setStartRow(startRow.getBytes());
			scan.setStopRow(stopRow.getBytes());
			ResultScanner resultScanner = table.getScanner(scan);
			return new IteratorHelper<>(resultScanner.iterator(), helper).toList();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public void salvar(TransacaoBancaria transacaoBancaria) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			Put put = helper.put(transacaoBancaria);
			table.put(put);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public void salvar(List<TransacaoBancaria> transacoes) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			List<Put> puts = new ArrayList<>();
			transacoes.forEach(e -> puts.add(helper.put(e)));
			table.put(puts);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isTableExists() {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			return helper.isTableExists(connection);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
