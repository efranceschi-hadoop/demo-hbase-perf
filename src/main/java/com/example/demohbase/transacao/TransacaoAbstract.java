package com.example.demohbase.transacao;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
abstract class TransacaoAbstract implements Transacao {

	private String id;

	private String cpf;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date data;

	private String descricao;

	private BigDecimal valor;

}
