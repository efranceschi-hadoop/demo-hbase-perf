package com.example.demohbase.transacao;

import com.example.demohbase.utils.ByteUtils;
import com.example.demohbase.utils.IteratorHelper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Profile("timestamp")
@Qualifier("transacaoBancariaRepository")
@Repository
public class TransacaoBancariaTSRepositoryImpl implements TransacaoRepository<TransacaoBancaria> {

	private final Configuration configuration;
	private final TransacaoBancariaHelper helper;

	public TransacaoBancariaTSRepositoryImpl(
			Configuration configuration,
			TransacaoBancariaHelper helper
	) {
		this.configuration = configuration;
		this.helper = helper;
	}

	@SuppressWarnings("Duplicates")
	@Override
	public List<TransacaoBancaria> consultar(String cpf, Contrato contrato, Date dataInicio, Date dataTermino) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			ContratoBancario contratoBancario = (ContratoBancario) contrato;
			String startRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.agencia(contratoBancario.getAgencia())
					.conta(contratoBancario.getConta())
					.build()
			);
			String stopRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.agencia(contratoBancario.getAgencia())
					.conta(contratoBancario.getConta())
					.build()
			);
			Scan scan = new Scan();
			scan.setStartRow(startRow.getBytes());
			scan.setStopRow(ByteUtils.join(stopRow.getBytes(), new byte[] { Byte.MAX_VALUE } ));
			scan.setTimeRange(dataInicio.getTime(), dataTermino.getTime());
			ResultScanner resultScanner = table.getScanner(scan);
			return new IteratorHelper<>(resultScanner.iterator(), helper).toList();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public List<TransacaoBancaria> consultar(String cpf, Date dataInicio, Date dataTermino) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			String startRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.build()
			);
			String stopRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.build()
			);
			Scan scan = new Scan();
			scan.setStartRow(startRow.getBytes());
			scan.setStopRow(ByteUtils.join(stopRow.getBytes(), new byte[] { Byte.MAX_VALUE } ));
			scan.setTimeRange(dataInicio.getTime(), dataTermino.getTime());
			ResultScanner resultScanner = table.getScanner(scan);
			return new IteratorHelper<>(resultScanner.iterator(), helper).toList();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public void salvar(TransacaoBancaria transacaoBancaria) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			Put put = helper.put(transacaoBancaria);
			table.put(put);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public void salvar(List<TransacaoBancaria> transacoes) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			List<Put> puts = new ArrayList<>();
			transacoes.forEach(e -> puts.add(helper.put(e)));
			table.put(puts);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isTableExists() {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			return helper.isTableExists(connection);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
