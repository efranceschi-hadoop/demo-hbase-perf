package com.example.demohbase.transacao;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-04.
 */
@Profile("timestamp")
@Component
public class TransacaoBancariaTSHelper extends TransacaoAbstractHelper<TransacaoBancaria> implements TransacaoBancariaHelper {

	private static final String TABLE_NAME = "extrato_ts";

	private static final String CF_INFO = "i";

	public TransacaoBancariaTSHelper() {
		super(TABLE_NAME);
	}

	@SuppressWarnings("Duplicates")
	@Override
	public TransacaoBancaria value(Result result) {
		return TransacaoBancaria
				.builder()
				.cpf(Bytes.toString(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("cpf"))))
				.data(new Date(Bytes.toLong(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("data")))))
				.agencia(Bytes.toString(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("ag"))))
				.conta(Bytes.toString(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("cc"))))
				.id(Bytes.toString(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("id"))))
				.descricao(Bytes.toString(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("desc"))))
				.valor(Bytes.toBigDecimal(result.getValue(CF_INFO.getBytes(), Bytes.toBytes("val"))))
				.build();
	}

	@SuppressWarnings("Duplicates")
	@Override
	public String getRowKey(TransacaoBancaria transacaoBancaria) {
		StringBuilder result = new StringBuilder()
				.append(StringUtils.reverse(transacaoBancaria.getCpf()));

		if (transacaoBancaria.getConta() != null && transacaoBancaria.getAgencia() != null) {
			result
					.append(":")
					.append(StringUtils.reverse(transacaoBancaria.getConta()))
					.append(StringUtils.reverse(transacaoBancaria.getAgencia()));

			if (transacaoBancaria.getId() != null) {
				result
						.append(":")
						.append(StringUtils.leftPad(transacaoBancaria.getId(), 15, '0'));
			}
		}

		return result.toString();
	}

	@SuppressWarnings("Duplicates")
	@Override
	public Put put(TransacaoBancaria transacaoBancaria) {
		String rowkey = getRowKey(transacaoBancaria);
		Put put = new Put(rowkey.getBytes(), transacaoBancaria.getData().getTime());
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("cpf"), Bytes.toBytes(transacaoBancaria.getCpf()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("data"), Bytes.toBytes(transacaoBancaria.getData().getTime()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("ag"), Bytes.toBytes(transacaoBancaria.getAgencia()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("cc"), Bytes.toBytes(transacaoBancaria.getConta()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("id"), Bytes.toBytes(transacaoBancaria.getId()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("desc"), Bytes.toBytes(transacaoBancaria.getDescricao()));
		put.addColumn(CF_INFO.getBytes(), Bytes.toBytes("val"), Bytes.toBytes(transacaoBancaria.getValor()));
		return put;
	}

}
