package com.example.demohbase.transacao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Controller
public class SomaController {

	private static final Logger LOG = LoggerFactory.getLogger(SomaController.class);

	private final ObjectMapper om = new ObjectMapper();
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private final TransacaoService transacaoService;

	public SomaController(TransacaoService transacaoService) {
		this.transacaoService = transacaoService;
	}

	// Situação 1: Soma por CPF
	@SuppressWarnings("Duplicates")
	@GetMapping(value = "/soma/{cpf}", produces = "text/json")
	@ResponseBody
	public String soma_CPF(
			@PathVariable("cpf") String cpf,
			@RequestParam(value = "dataInicio", required = false, defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dataInicio,
			@RequestParam(value = "dataTermino", required = false, defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dataTermino
	) throws JsonProcessingException {

		dataInicio = configuraDataInicio(dataInicio);
		dataTermino = configuraDataTermino(dataTermino);

		long tempo = System.currentTimeMillis();
		List<TransacaoBancaria> transacoes = transacaoService.consultarTransacaoBancaria(cpf, dataInicio, dataTermino);
		LOG.debug("Tempo de consulta no HBASE: {} ms", System.currentTimeMillis() - tempo);

		return createResult(cpf, dataInicio, dataTermino, transacoes);
	}

	// Situação 2: Soma por CPF + AGENCIA + CONTA
	@SuppressWarnings("Duplicates")
	@GetMapping(value = "/soma/{cpf}/{agencia}/{conta}", produces = "text/json")
	@ResponseBody
	public String soma_CPF_Agencia_Conta(
			@PathVariable("cpf") String cpf,
			@PathVariable("agencia") String agencia,
			@PathVariable("conta") String conta,
			@RequestParam(value = "dataInicio", required = false, defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dataInicio,
			@RequestParam(value = "dataTermino", required = false, defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dataTermino
	) throws JsonProcessingException {

		dataInicio = configuraDataInicio(dataInicio);
		dataTermino = configuraDataTermino(dataTermino);

		long tempo = System.currentTimeMillis();
		List<TransacaoBancaria> transacoes = transacaoService.consultarTransacaoBancaria(cpf, agencia, conta, dataInicio, dataTermino);
		LOG.debug("Tempo de consulta no HBASE: {} ms", System.currentTimeMillis() - tempo);

		return createResult(cpf, dataInicio, dataTermino, transacoes);
	}

	@SuppressWarnings("Duplicates")
	private String createResult(String cpf, Date dataInicio, Date dataTermino, List<TransacaoBancaria> transacoes) throws JsonProcessingException {

		long tempo = System.currentTimeMillis();
		BigDecimal saldo = BigDecimal.ZERO;
		for (Transacao t : transacoes) {
			saldo = saldo.add(t.getValor());
		}
		LOG.debug("Tempo para soma das transações: {} ms", System.currentTimeMillis() - tempo);

		tempo = System.currentTimeMillis();
		try {
			Map<String, Object> result = new TreeMap<>();
			result.put("cpf", cpf);
			result.put("dataInicio", sdf.format(dataInicio));
			result.put("dataTermino", sdf.format(dataTermino));
			result.put("saldo", saldo);
			result.put("count", transacoes.size());
			return om.writeValueAsString(result);
		} finally {
			LOG.debug("Tempo para construir json: {} ms", System.currentTimeMillis() - tempo);
		}
	}

	private Date configuraDataTermino(Date dataTermino) {
		if (dataTermino == null) {
			dataTermino = new Date();
		}
		return dataTermino;
	}

	@SuppressWarnings("Duplicates")
	private Date configuraDataInicio(Date dataInicio) {
		if (dataInicio == null) {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, -1);
			dataInicio = c.getTime();
		}
		return dataInicio;
	}

}
