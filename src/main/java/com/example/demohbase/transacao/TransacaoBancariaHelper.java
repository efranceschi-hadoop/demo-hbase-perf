package com.example.demohbase.transacao;

import com.example.demohbase.utils.HBaseHelper;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-28.
 */
public interface TransacaoBancariaHelper extends HBaseHelper<TransacaoBancaria> {
}
