package com.example.demohbase.transacao;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-26.
 */
@Data
@AllArgsConstructor
class ContratoCartaoCredito implements Contrato {
	private String numeroCartao;
}
