package com.example.demohbase.transacao;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-25.
 */
interface Transacao {

	String getId();

	String getCpf();

	String getDescricao();

	Date getData();

	BigDecimal getValor();

}
