package com.example.demohbase.utils;

import org.apache.commons.lang.CharUtils;

import java.util.Arrays;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-27.
 */
public final class ByteUtils {

	public static final byte ZERO = 0;
	public static final byte ONE = 1;

	public static byte[] repeat(byte b, int times) {
		byte[] result = new byte[times];
		Arrays.fill(result, b);
		return result;
	}

	public static byte[] join(byte[]... arrays) {
		int totalSize = 0;
		for (byte[] array : arrays) {
			totalSize += array.length;
		}

		byte[] result = new byte[totalSize];
		int pos = 0;

		for (byte[] array : arrays) {
			System.arraycopy(array, 0, result, pos, array.length);
			pos += array.length;
		}

		return result;
	}

	public static String prettyPrint(byte[] key) {
		char[] str = new char[key.length];
		for (int i = 0; i < key.length; i++) {
			str[i] = CharUtils.isAsciiPrintable((char) key[i]) ? (char) key[i] : '?';
		}
		return new String(str);
	}

	public static String toMask(byte[] mask) {
		char[] str = new char[mask.length];
		for (int i = 0; i < mask.length; i++) {
			str[i] = mask[i] == 0 ? '0' : (mask[i] == 1 ? '1' : '?');
		}
		return new String(str);
	}

	public static byte toByte(int b) {
		return (byte) b;
	}
}
