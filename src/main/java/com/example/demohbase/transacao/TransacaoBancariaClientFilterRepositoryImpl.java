package com.example.demohbase.transacao;

import com.example.demohbase.utils.IteratorHelper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-03.
 */
@Profile("clientfilter")
@Qualifier("transacaoBancariaRepository")
@Repository
public class TransacaoBancariaClientFilterRepositoryImpl implements TransacaoRepository<TransacaoBancaria> {

	private static final Logger LOG = LoggerFactory.getLogger(TransacaoBancariaClientFilterRepositoryImpl.class);

	private final Configuration configuration;
	private final TransacaoBancariaHelper helper;

	public TransacaoBancariaClientFilterRepositoryImpl(
			Configuration configuration,
			TransacaoBancariaHelper helper
	) {
		this.configuration = configuration;
		this.helper = helper;
	}

	@SuppressWarnings("Duplicates")
	@Override
	public List<TransacaoBancaria> consultar(String cpf, Contrato contrato, Date dataInicio, Date dataTermino) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			ContratoBancario contratoBancario = (ContratoBancario) contrato;

			String startRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataInicio)
					.build()
			);
			String stopRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataTermino)
					.build()
			);

			Scan scan = new Scan();
			scan.setStartRow(startRow.getBytes());
			scan.setStopRow(stopRow.getBytes());

			ResultScanner resultScanner = table.getScanner(scan);

			IteratorHelper<TransacaoBancaria> resultIterator = new IteratorHelper<>(resultScanner.iterator(), helper);
			List<TransacaoBancaria> resultList = new ArrayList<>();
			while (resultIterator.hasNext()) {
				TransacaoBancaria t = resultIterator.next();
				if (t.getAgencia().equals(contratoBancario.getAgencia()) && t.getConta().equals(contratoBancario.getConta())) {
					resultList.add(t);
				}
			}
			return resultList;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public List<TransacaoBancaria> consultar(String cpf, Date dataInicio, Date dataTermino) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			String startRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataInicio)
					.build()
			);
			String stopRow = helper.getRowKey(TransacaoBancaria
					.builder()
					.cpf(cpf)
					.data(dataTermino)
					.build()
			);
			Scan scan = new Scan();
			scan.setStartRow(startRow.getBytes());
			scan.setStopRow(stopRow.getBytes());
			ResultScanner resultScanner = table.getScanner(scan);
			return new IteratorHelper<>(resultScanner.iterator(), helper).toList();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public void salvar(TransacaoBancaria transacaoBancaria) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			Put put = helper.put(transacaoBancaria);
			table.put(put);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("Duplicates")
	@Override
	public void salvar(List<TransacaoBancaria> transacoes) {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			Table table = helper.getTable(connection);
			List<Put> puts = new ArrayList<>();
			transacoes.forEach(e -> puts.add(helper.put(e)));
			table.put(puts);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isTableExists() {
		try (Connection connection = ConnectionFactory.createConnection(configuration)) {
			return helper.isTableExists(connection);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
