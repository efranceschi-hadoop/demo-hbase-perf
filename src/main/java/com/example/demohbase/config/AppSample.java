package com.example.demohbase.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Eduardo Franceschi
 * @since 2019-03-29.
 */
@Component
@ConfigurationProperties(prefix = "app.sample")
@Data
public class AppSample {
	private int cpf;
	private int agencias;
	private int contas;
	private int periodo;
	private int transacoesDia;
}
